#!/usr/bin/python

"""libsaas gitlab
=================

Extension to libsaas library
"""
import os
from distutils.core import setup, Command
from setuptools import setup, find_packages

VERSION = (0, 4, 0)

__version__     = ".".join(map(str, VERSION[0:3]))

try:
    __version__ += "." + os.environ["VERSION_APPEND"]
except:
    pass

__description__ = "Python wrapper for GitLab API v4"
__author__      = "b-sh"
__contact__     = "b-sh@gmx.net"
__homepage__    = "https://gitlab.com/bor-sh-infrastructure/libsaas_gitlab"
__copyright__   = ""
__license__     = "MIT"

install_requires = ['libsaas==0.4']

class Test(Command):
    description = "run the automated test suite"
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        from test.run_tests import main
        if not main().wasSuccessful():
            raise SystemExit(1)

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
      name="libsaas_gitlab",
      version=__version__,
      description=__description__,
      author=__author__,
      author_email=__contact__,
      url=__homepage__,
      license=__license__,
      packages=find_packages(),
      install_requires=install_requires,
      long_description=long_description,
      long_description_content_type="text/markdown",
      classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3",
        "Topic :: Database",
        "Topic :: Software Development :: Libraries :: Python Modules",
        ],
      platforms=["any"],
      cmdclass={'test': Test}
)
